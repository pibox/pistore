/*******************************************************************************
 * pistore
 *
 * scanner.c:  Scan file systems and populate the db.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define SCANNER_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/inotify.h>
#include <mntent.h>
#include <linux/limits.h>

#include "pistore.h"

struct scanpath {
    char    *path;
    int     iter;
};

static int scannerIsRunning = 0;
static pthread_mutex_t scannerProcessorMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_t scannerProcessorThread;
static int inotify_fd = -1;     // inotify file descriptor

/* Local Prototypes */
int scanFS( char *path );

/*
 ******************************************************************************
 ******************************************************************************
 *
 * Thread functions
 *
 ******************************************************************************
 ******************************************************************************
 */

/*
 *========================================================================
 * Name:   initWatcher
 * Prototype:  int initWatcher( void )
 *
 * Description:
 * Setup to watch directories under MOUNT_DIR for changes that will cause us to update the db.
 *
 * Returns:
 * inotify_fd on success (inotify is enabled).
 * -1 on failure (inotify is not enabled).
 *========================================================================
 */
static int 
initWatcher ( void )
{
    int     wd;

    piboxLogger(LOG_INFO, "Entered.\n");

    /* Open an inotify instance */
    inotify_fd = inotify_init();
    if (inotify_fd < 0)
    {
        piboxLogger(LOG_ERROR, "inotify_init(): %s\n", strerror(errno));
        return -1;
    }

    /* Watch for the creation and deletion of directories. */
    wd = inotify_add_watch (inotify_fd, MOUNT_DIR, IN_CREATE | IN_DELETE);
    if (wd < 0)
    {
        piboxLogger(LOG_ERROR, "Cannot add watch for \"%s\" with event mask %lX", MOUNT_DIR, IN_CREATE | IN_DELETE);
        close(inotify_fd);
        inotify_fd = -1;
        return -1;
    }

    piboxLogger(LOG_INFO, "Initialized watch directories.\n");
    return inotify_fd;
}

/*
 *========================================================================
 * Name:   stopWatchDirs
 * Prototype:  void stopWatchDirs( void )
 *
 * Description:
 * Stop watching directories for changes.
 *========================================================================
 */
void 
stopWatchDirs ( void )
{
    /* Open an inotify instance */
    if ( inotify_fd != -1 )
    {
        close(inotify_fd);
        inotify_fd = -1;
    }
}

/*========================================================================
 * Name:   isScannerProcessorRunning
 * Prototype:  int isScannerProcessorRunning( void )
 *
 * Description:
 * Thread-safe read of scannerIsRunning variable.
 *========================================================================*/
int
isScannerProcessorRunning( void )
{
    int status;
    pthread_mutex_lock( &scannerProcessorMutex );
    status = scannerIsRunning;
    pthread_mutex_unlock( &scannerProcessorMutex );
    return status;
}

/*========================================================================
 * Name:   setScannerProcessorRunning
 * Prototype:  int setScannerProcessorRunning( void )
 *
 * Description:
 * Thread-safe set of scannerIsRunning variable.
 *========================================================================*/
static void
setScannerProcessorRunning( int val )
{
    pthread_mutex_lock( &scannerProcessorMutex );
    scannerIsRunning = val;
    pthread_mutex_unlock( &scannerProcessorMutex );
}

/*
 * ========================================================================
 * Name:   scanTimer
 * Prototype:  gboolean scanTimer( gpointer data )
 *
 * Description:
 * Scans for a mount point a few seconds after the directory for the mount
 * shows up.  Retries up to 5 times then just gives up.
 * ========================================================================
 */
static gboolean 
scanTimer(gpointer data)
{
    struct scanpath *scanpath = (struct scanpath *)data;

    if ( scanpath == NULL )
    {
        piboxLogger(LOG_ERROR, "Missing scanpath struct pointer.\n");
        return FALSE;
    }

    if ( scanpath->path == NULL )
    {
        piboxLogger(LOG_ERROR, "Missing scanpath path.\n");
        free(scanpath);
        return FALSE;
    }

    if ( scanFS(scanpath->path) != 0  )
    {
        scanpath->iter++;
        if ( scanpath->iter < 5 )
        {
            /* Not there yet - try again later. */
            return TRUE;
        }
        else
        {
            free(scanpath->path);
            free(scanpath);
        }
    }
    else
    {
        free(scanpath->path);
        free(scanpath);
    }
    return FALSE;
}

/*========================================================================
 * Name:   scannerProcessor
 * Prototype:  void scannerProcessor( CLI_T * )
 *
 * Description:
 * Wait on changes to inotify file descriptor.  When they happen,
 * update the database as appropriate.
 *
 * Input Arguments:
 * void *arg    Unused, but required by API.
 *
 * Notes:
 * This thread starts once and waits for the scanner to exit.
 *========================================================================*/
static void *
scannerProcessor( void *arg )
{
    int             scannerfd = -1;
    ssize_t         len;
    fd_set          rfds;
    struct timeval  tv;
    int             retval;
    char            *ptr;
    char            buf[sizeof(struct inotify_event)+NAME_MAX+1];
    char            path[PATH_MAX];
    struct scanpath *scanPath;

    const struct inotify_event  *event;

    /* Mark the processor loop as running. */
    setScannerProcessorRunning(1);

    /* Setup inotify from db.c */
    scannerfd = initWatcher();
    if ( scannerfd == -1 )
    {
        piboxLogger(LOG_ERROR, "Can't watch db directories.\n");
        goto scannerExit;
    }

    /* Set loop to continue running. */
    setCLIFlag(CLI_SCANNER);
    while( isCLIFlagSet(CLI_SCANNER) )
    {   
        /* Watch the timer fd for activity. */
        FD_ZERO(&rfds);
        FD_SET(scannerfd, &rfds);

        /*
         * Reset select()'s timeout on each run.
         */
        tv.tv_sec = 1;
        tv.tv_usec = 0;
        retval = select(scannerfd+1, &rfds, NULL, NULL, &tv);

        /* A select timeout with no fds or an error just makes us wait again, unless CLI_SCANNER has been disabled. */
        if ( retval == 0 )
        {   
            piboxLogger(LOG_TRACE4, "select: timeout\n");
            continue;
        }
        if ( retval < 0 )
        {   
            piboxLogger(LOG_TRACE1, "error on select(): %s\n", strerror(errno));
            continue;
        }

        /* Read the event. */
        len = read(scannerfd, buf, sizeof buf);

        /* Loop over all events for files/dir changes, updating the db as needed. */
        piboxLogger(LOG_INFO, "Processing inotify events.\n");
        for (ptr = buf; ptr < buf + len; ptr += sizeof(struct inotify_event) + event->len)
        {   
            event = (const struct inotify_event *) ptr;

            /* If no name in event, skip it. */
            if ( event->len == 0 )
            {
                piboxLogger(LOG_INFO, "Inotify event for %s - no name provided. Skiping.\n", MOUNT_DIR);
                continue;
            }

            piboxLogger(LOG_INFO, "Inotify event for %s/%s\n", MOUNT_DIR, event->name);

            /* Skip special files. */
            if ( strcmp(event->name,".") == 0 )
            {
                piboxLogger(LOG_INFO, "Inotify event for %s on \".\" Skiping.\n", MOUNT_DIR);
                continue;
            }
            else if ( strcmp(event->name, "..") == 0 )
            {
                piboxLogger(LOG_INFO, "Inotify event for %s on \"..\" Skiping.\n", MOUNT_DIR);
                continue;
            }

            piboxLogger(LOG_INFO, "Inotify event for %s/%s\n", MOUNT_DIR, event->name);

            memset(path, 0, MAXBUF);
            snprintf(path, MAXBUF-1, "%s/%s", MOUNT_DIR, event->name);
            if ( event->mask & IN_CREATE )
            {
                piboxLogger(LOG_INFO, "Create event for %s/%s\n", MOUNT_DIR, event->name);
                scanPath = (struct scanpath *)calloc(1,sizeof(struct scanpath));
                scanPath->path = (char *)calloc(1,strlen(path)+1);
                strcpy(scanPath->path, path);

                /* scanTimer must free up the allocated storage! */
                (void)g_timeout_add(3000, (GSourceFunc)scanTimer, (gpointer)scanPath);
            }
            if ( event->mask & IN_DELETE )
            {
                piboxLogger(LOG_INFO, "Delete event for %s/%s\n", MOUNT_DIR, event->name);
                dbRemoveFS(path);
            }
        }
    }
    /* Close up inotify fd */
    stopWatchDirs();

scannerExit:
    piboxLogger(LOG_INFO, "Scanner thread is exiting.\n");
    unsetCLIFlag(CLI_SCANNER);
    setScannerProcessorRunning(0);
    return(0);
}

/*========================================================================
 * Name:   startScannerProcessor
 * Prototype:  void startScannerProcessor( void )
 *
 * Description:
 * Setup thread to handle inbound messages.
 *
 * Notes:
 * Threads run until configs->serverEnabled = 0.  See shutdownScannerProcessor().
 *========================================================================*/
void
startScannerProcessor( void )
{
    int rc;

    /* Prevent running two at once. */
    if ( isScannerProcessorRunning() )
        return;

    /* Create a thread to expire streams. */
    rc = pthread_create(&scannerProcessorThread, NULL, &scannerProcessor, NULL);
    if (rc)
    {
        piboxLogger(LOG_ERROR, "%s: Failed to create scanner thread: %s\n", PROG, strerror(rc));
        exit(-1);
    }
    piboxLogger(LOG_INFO, "%s: Started scanner thread.\n", PROG);
    return;
}

/*========================================================================
 * Name:   shutdownScannerProcessor
 * Prototype:  void shutdownScannerProcessor( void )
 *
 * Description:
 * Shut down message processing thread.
 *========================================================================*/
void
shutdownScannerProcessor( void )
{
    int timeOut = 0;

    /* Disable scanner loop */
    unsetCLIFlag(CLI_SCANNER);

    /* Wait for thread to exit. */
    if ( isScannerProcessorRunning() )
    {
        while ( isScannerProcessorRunning() )
        {
            sleep(1);
            timeOut++;
            if (timeOut == 60)
            {
                piboxLogger(LOG_ERROR, "Timed out waiting on scanner thread to shut down.\n");
                return;
            }
        }
        pthread_detach(scannerProcessorThread);
    }

    piboxLogger(LOG_INFO, "scannerProcessor shut down.\n");
}

/*========================================================================
 * Name:   scanFS
 * Prototype:  int scanFS( char *path )
 *
 * Description:
 * Scan of mount points.
 *
 * Input Arguments:
 * char *path       If NULL, process all of /proc/mounts.
 *                  If not NULL, process path if found in /proc/mounts.
 *
 * Returns:
 * 0                Scan succeeded for new mount point.
 * 1                Scan failed for requested path.
 *
 * Notes:
 * /proc/mounts SHOULD be updated with path by the time this is called.
 *========================================================================*/
int
scanFS( char *path )
{
    struct mntent   *entry;
    FILE            *fd;
    int             rc = 1;

    fd = setmntent("/proc/mounts", "r");
    if (fd == NULL)
    {
        piboxLogger(LOG_ERROR, "Can't read /proc/mounts\n");
        return(rc);
    }

    if ( path != NULL )
    {
        if (strncmp(path, MOUNT_DIR, strlen(MOUNT_DIR)) != 0 )
        {
            piboxLogger(LOG_ERROR, "Requested path not in %s: %s\n", MOUNT_DIR, path);
            return(rc);
        }
    }

    /* Iterate over mounted file systems */
    while (NULL != (entry = getmntent(fd)))
    {
        piboxLogger(LOG_INFO, "Testing *%s* against *%s*\n", entry->mnt_dir, path);
        if ( path != NULL )
        {
            if (strcmp(path, entry->mnt_dir) != 0 )
            {
                piboxLogger(LOG_INFO, "No match.\n");
                continue;
            }
            piboxLogger(LOG_INFO, "They match!\n");
        }

        piboxLogger(LOG_INFO, "Found path %s in mount entries.\n", path);

        if (strncmp(entry->mnt_dir, MOUNT_DIR, strlen(MOUNT_DIR)) != 0 )
        {
            piboxLogger(LOG_INFO, "Not a managed mount point: %s\n", entry->mnt_dir);
            continue;
        }

        // Only process certain types of file systems.
        if( (strcmp(entry->mnt_type, MNTTYPE_IGNORE) == 0) &&
            (strcmp(entry->mnt_type, MNTTYPE_SWAP  ) == 0) )
        {
            piboxLogger(LOG_INFO, "Skipping IGNORE/SWAP mount type: %s\n", entry->mnt_dir);
            continue;
        }

        piboxLogger(LOG_INFO, "Adding managed mount point: %s\n", entry->mnt_dir);
        dbUpdateFS(entry);
        rc=0;
    }
    endmntent(fd);
    return(rc);
}

