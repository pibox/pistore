/*******************************************************************************
 * pistore
 *
 * statuswidget.h:  custom widget for showing file status data
 *
 * License: BSD0
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 * 
 * Based on:
 * Custom GTK+ widget - http://zetcode.com/tutorials/gtktutorial/customwidget/
 * Clock Widget - https://github.com/humbhenri/clocks/tree/master/gtk-clock
 ******************************************************************************/
#define STATUSWIDGET_C

#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <glib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/vfs.h>
#include <pibox/log.h>
#include "statuswidget.h"
#include "cli.h"
#include "db.h"

static gint largeFont  = 25;
static gint mediumFont = 18;
// static gint smallFont  = 10;

static pthread_mutex_t widgetMutex = PTHREAD_MUTEX_INITIALIZER;

/*
 * Local copy of widget structure so some callbacks can
 * access it.
 */
static GtkStatus *local_status = NULL;
static guint timeout = 0;

/*
 *========================================================================
 * Prototypes
 *========================================================================
 */
static void gtk_status_class_init(GtkStatusClass *klass);
static void gtk_status_init(GtkStatus *status);
static void gtk_status_size_request(GtkWidget *widget, GtkRequisition *requisition);
static void gtk_status_size_allocate(GtkWidget *widget, GtkAllocation *allocation);
static void gtk_status_realize(GtkWidget *widget);
static gboolean gtk_status_expose(GtkWidget *widget, GdkEventExpose *event);
static void gtk_status_paint(GtkWidget *widget);
static void gtk_status_destroy(GtkObject *object);

/*
 *========================================================================
 *========================================================================
 * Private API
 *========================================================================
 *========================================================================
 */
GtkType
gtk_status_get_type(void)
{
    static GtkType gtk_status_type = 0;
    if (!gtk_status_type) {
        static const GtkTypeInfo gtk_status_info = {
            "GtkStatus",
            sizeof(GtkStatus),
            sizeof(GtkStatusClass),
            (GtkClassInitFunc) gtk_status_class_init,
            (GtkObjectInitFunc) gtk_status_init,
            NULL,
            NULL,
            (GtkClassInitFunc) NULL
        };
        gtk_status_type = gtk_type_unique(GTK_TYPE_WIDGET, &gtk_status_info);
    }
    return gtk_status_type;
}

GtkWidget * gtk_status_new()
{
    return GTK_WIDGET(gtk_type_new(gtk_status_get_type()));
}

static void
gtk_status_class_init(GtkStatusClass *klass)
{
    GtkWidgetClass *widget_class;
    GtkObjectClass *object_class;

    widget_class = (GtkWidgetClass *) klass;
    object_class = (GtkObjectClass *) klass;

    widget_class->realize = gtk_status_realize;
    widget_class->size_request = gtk_status_size_request;
    widget_class->size_allocate = gtk_status_size_allocate;
    widget_class->expose_event = gtk_status_expose;

    object_class->destroy = gtk_status_destroy;
}

static void
gtk_status_init(GtkStatus *status)
{
    status->stores = NULL;
    status->nodes = NULL;
    status->pixmap = NULL;
    local_status = status;
}

static void
gtk_status_size_request(GtkWidget *widget, GtkRequisition *requisition)
{
    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_STATUS(widget));
    g_return_if_fail(requisition != NULL);
}

static void
gtk_status_size_allocate(GtkWidget *widget, GtkAllocation *allocation)
{
    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_STATUS(widget));
    g_return_if_fail(allocation != NULL);

    widget->allocation = *allocation;
    if (GTK_WIDGET_REALIZED(widget)) {
        gdk_window_move_resize(
            widget->window,
            allocation->x, allocation->y,
            allocation->width, allocation->height
        );
    }
}

static void
gtk_status_realize(GtkWidget *widget)
{
    GdkWindowAttr attributes;
    guint attributes_mask;
    GtkStyle *style;
    GtkAllocation alloc;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_STATUS(widget));

    GTK_WIDGET_SET_FLAGS(widget, GTK_REALIZED);

    gtk_widget_get_allocation(widget, &alloc);

    piboxLogger(LOG_INFO, "alloc.{x,y,w,h}: %d,%d,%d,%d\n", 
            alloc.x, alloc.y, alloc.width, alloc.height);

    attributes.window_type = GDK_WINDOW_CHILD;
    attributes.x           = alloc.x;
    attributes.y           = alloc.y;
    attributes.width       = alloc.width;
    attributes.height      = alloc.height;
    attributes.wclass      = GDK_INPUT_OUTPUT;
    attributes.event_mask  = gtk_widget_get_events(widget) | GDK_EXPOSURE_MASK;
    attributes_mask        = GDK_WA_X | GDK_WA_Y;

    widget->window = gdk_window_new(
        gtk_widget_get_parent_window (widget),
        & attributes, attributes_mask
    );

    style = gtk_widget_get_style (widget);
    if (style != NULL) 
    {
        GTK_STATUS(widget)->bg = style->bg[GTK_STATE_NORMAL];
        GTK_STATUS(widget)->fg = style->fg[GTK_STATE_NORMAL];
    }

    gdk_window_set_user_data(widget->window, widget);
    widget->style = gtk_style_attach(widget->style, widget->window);
    gtk_style_set_background(widget->style, widget->window, GTK_STATE_NORMAL);
}

static gboolean
gtk_status_expose(GtkWidget *widget, GdkEventExpose *event)
{
    g_return_val_if_fail(widget != NULL, FALSE);
    g_return_val_if_fail(GTK_IS_STATUS(widget), FALSE);
    g_return_val_if_fail(event != NULL, FALSE);

    // Copy the entire pixmap over the widget's drawing area.
    if ( GTK_STATUS(widget)->pixmap != NULL )
    {
        gdk_draw_drawable(widget->window,
            widget->style->fg_gc[GTK_WIDGET_STATE(widget)], GTK_STATUS(widget)->pixmap,
            event->area.x, event->area.y,
            event->area.x, event->area.y,
            event->area.width, event->area.height);
    }
    return TRUE;
}

/*
 *========================================================================
 * Name:    freeStore
 * Prototype:   void freeStore( gpointer item )
 *
 * Description:
 * Iterator for freeing up the string for each store.
 *========================================================================
 */
static void
freeStore( gpointer item )
{
    if ( item != NULL )
        free(item);
}

/*
 *========================================================================
 * Name:    freeNodes
 * Prototype:   void freeNodes( gpointer item )
 *
 * Description:
 * Iterator for freeing up the string for each Nodes.
 *========================================================================
 */
static void
freeNodes( gpointer item )
{
    struct _statusnodes *node = (struct _statusnodes *)item;

    if ( node != NULL )
    {
        if (node->path)
            free(node->path);
        if (node->label)
            free(node->label);
        if (node->device)
            free(node->device);
        free(node);
    }
}

/*
 *========================================================================
 * Name:   findByPath
 * Prototype:  int findByPath( gconstpointer item, gconstpointer user_data )
 *
 * Description:
 * Find FS entry by mount point directory path.
 *========================================================================
 */
static int
findByPath( gconstpointer item, gconstpointer user_data )
{
    struct _statusnodes *node = (struct _statusnodes *)item;
    char *path          = (char *)user_data;

    if ( strcmp(node->path, path) == 0 )
        return 0;
    else
        return 1;
}

/*
 *========================================================================
 * Name:   drawText
 * Prototype:  void *drawText( cairo_t, int, int, char *, int )
 *
 * Description:
 * Updates text on the display.
 *========================================================================
 */
void
drawText( cairo_t *cr, int pixmap_width, int pixmap_height, char *textPtr, int multiplier)
{
    PangoFontDescription    *desc;
    PangoRectangle          pangoRectangle;
    PangoAttrList           *attrs = NULL;
    PangoLayout             *layout;
    char                    buf[256];
    gint                    fontSize = largeFont;

    if ( isCLIFlagSet( CLI_SMALL_SCREEN ) )
        fontSize = mediumFont;
    piboxLogger(LOG_TRACE3, "text size: %d\n", fontSize);

    layout = pango_cairo_create_layout(cr);
    sprintf(buf, "Monospace Bold %d", fontSize);
    desc = pango_font_description_from_string(buf);
    pango_font_description_set_absolute_size(desc, fontSize*PANGO_SCALE);
    pango_layout_set_font_description(layout, desc);
    pango_font_description_free(desc);
    pango_layout_set_wrap(layout, PANGO_WRAP_WORD);
    pango_layout_set_width(layout, ((int)(pixmap_width)-20)*PANGO_SCALE);
    pango_layout_set_spacing(layout, 1);
    pango_layout_set_single_paragraph_mode(layout, FALSE);
    pango_layout_set_alignment(layout, PANGO_ALIGN_LEFT);

    /* Initialize attributes. */
    attrs = pango_attr_list_new();
    pango_attr_list_insert (attrs, pango_attr_underline_new(PANGO_UNDERLINE_NONE));
    pango_layout_set_attributes (layout, attrs);

    piboxLogger(LOG_TRACE4, "text: %s\n", textPtr);
    pango_layout_set_text(layout, textPtr, -1);
    pango_layout_get_pixel_size(layout, &pangoRectangle.width, &pangoRectangle.height );
    piboxLogger(LOG_TRACE4, "text w/h: %d / %d\n", pangoRectangle.width, pangoRectangle.height);
    cairo_set_source_rgb(cr, 0.9, 0.9, 1.0);
    pango_cairo_update_layout(cr, layout);

    if ( isCLIFlagSet( CLI_SMALL_SCREEN ) )
        cairo_translate(cr, (pixmap_width/2)-(pangoRectangle.width/2), (pixmap_height/2)-((pangoRectangle.height*multiplier)-5)/2);
    else
        cairo_translate(cr, (pixmap_width/2)-(pangoRectangle.width/2), (pixmap_height/2)-((pangoRectangle.height*multiplier)+25)/2);

    pango_cairo_show_layout(cr, layout);

    /* Cleanup pango */
    pango_attr_list_unref(attrs);
    g_object_unref(layout);
}

/*
 *========================================================================
 * Name:    gtk_status_paint
 * Prototype:   void gtk_status_paint( GtkWidget *widget )
 *
 * Description:
 * Handle painting (data update) of the widget.
 *========================================================================
 */
static void
gtk_status_paint(GtkWidget *widget)
{
    static int              painting = 0;
    int                     width, height;
    cairo_t                 *cr_pixmap;
    cairo_surface_t         *cst;
    cairo_t                 *cr;
    GdkPixmap               *pixmap;
    char                    *data, *ptr, buf[6];
    struct _statusnodes     *node;
    int                     idx;

    /* Avoid multiple calls */
    if ( painting )
        return;

    painting = 1;
    piboxLogger(LOG_TRACE2, "Entered\n");

    if ( GTK_STATUS(widget)->pixmap != NULL )
        g_object_unref( GTK_STATUS(widget)->pixmap );
    GTK_STATUS(widget)->pixmap = 
        gdk_pixmap_new(widget->window,widget->allocation.width,widget->allocation.height,-1);
    pixmap = GTK_STATUS(widget)->pixmap;

    piboxLogger(LOG_TRACE2, "Getting cr\n");
    gdk_drawable_get_size(pixmap, &width, &height);
    piboxLogger(LOG_TRACE2, "window w/h: %d / %d\n", width, height);
    cst = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, width, height);
    cr = cairo_create(cst);

    // Fill with black background with the default color
    piboxLogger(LOG_TRACE2, "Paint the background.\n");
    cairo_set_source_rgb(cr, 0, 0, 0);
    cairo_rectangle(cr, 0, 0, width, height);
    cairo_fill(cr);

    cairo_select_font_face(cr, "Purisa",
        CAIRO_FONT_SLANT_NORMAL,
        CAIRO_FONT_WEIGHT_BOLD);

    /* Build the text to display. */
    data = (char *)calloc(1, strlen(STORES_HDR) + 2);
    sprintf(data, "%s\n", STORES_HDR);
    for (idx=0; idx<g_slist_length(local_status->nodes); idx++)
    {
        node = g_slist_nth_data(local_status->nodes, idx);

        memset(buf, 0, 6);
        if ( strlen(node->label) >= 5 )
        {
            piboxLogger(LOG_INFO, "label >= 5\n");
            memcpy(buf, node->label, 5);
        }
        else
        {
            piboxLogger(LOG_INFO, "label < 5 (%d)\n", strlen(node->label));
            memcpy(buf, node->label, strlen(node->label));
            memset((char *)(buf + strlen(node->label)), ' ', 5-strlen(node->label));
        }
        piboxLogger(LOG_INFO, "label buf = *%s*\n", buf);

        ptr = (char *)calloc(1, strlen(data) + strlen(node->path) + strlen(buf) + 9);
        sprintf(ptr, "%s\n%s  %s  %s %s", 
                data,
                (node->flags & F_RO_STAMP)?"Y":"N",
                (node->flags & F_PS_STAMP)?"Y":"N",
                buf,
                node->path
                );
        piboxLogger(LOG_INFO, "ptr buf = *%s*\n", ptr);
        free(data);
        data = ptr;
    }
    drawText(cr, width, height, data, 1);
    free(data);

    /* Don't need the cairo object now */
    cairo_destroy(cr);

    cr_pixmap = gdk_cairo_create(pixmap);
    cairo_set_source_surface (cr_pixmap, cst, 0, 0);
    cairo_paint(cr_pixmap);
    cairo_destroy(cr_pixmap);
    cairo_surface_destroy(cst);

    painting = 0;
}

/*
 *========================================================================
 * Name:    gtk_status_destroy
 * Prototype:   void gtk_status_destroy( GtkWidget *widget )
 *
 * Description:
 * Clean up widget on destroy.
 *========================================================================
 */
static void
gtk_status_destroy(GtkObject *object)
{
    // GtkStatus *status;
    GtkStatusClass *klass;

    if ( timeout != 0 )
    {
        g_source_remove(timeout);
        timeout = 0;
    }

    g_return_if_fail(object != NULL);
    g_return_if_fail(GTK_IS_STATUS(object));

    // status = GTK_STATUS(object);
    klass = gtk_type_class(gtk_widget_get_type());
    if (GTK_OBJECT_CLASS(klass)->destroy) {
        (* GTK_OBJECT_CLASS(klass)->destroy) (object);
    }
}

/*
 *========================================================================
 * Name:    gtk_status_update
 * Prototype:   void gtk_status_update( GtkStatus *status )
 *
 * Description:
 * Update the statuss based on current list of stores.
 *======================================================================== */
void
gtk_status_update( GtkStatus *status )
{
    GdkRegion *region;

    pthread_mutex_lock( &widgetMutex );
    gtk_status_paint(GTK_WIDGET(status));
    region = gdk_drawable_get_clip_region(GTK_WIDGET(status)->window);
    gdk_window_invalidate_region(GTK_WIDGET(status)->window, region, TRUE);
    gdk_window_process_updates(GTK_WIDGET(status)->window, TRUE);
    pthread_mutex_unlock( &widgetMutex );
}

/*
 * ========================================================================
 * Name:   timer_exec
 * Prototype:  gboolean timer_exec( GtkWidget *window )
 *
 * Description:
 * Animation engine - causes updates for the animation on a timed interval.
 *
 * Notes:
 * Taken basically verbatim from the Cairo web site tutorial on animation.
 * ========================================================================
 */
static gboolean 
timer_exec(GtkWidget * window)
{
    /* Update the display. */
    gtk_status_update(local_status);
    return TRUE;
}

/*
 *========================================================================
 *========================================================================
 * Public API
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:    gtk_status_set
 * Prototype:   void gtk_status_set( GtkStatus *Status, GSList *stores )
 *
 * Description:
 * Set the list of stores to Status.
 *========================================================================
 */
void
gtk_status_set( GtkStatus *status, GSList *stores )
{
    char            *entry, *name, *totalStr, *availStr, *savePtr;
    char            *ro_flag, *ps_flag, *label;
    GSList          *listitem;
    int             idx;

    struct _statusnodes   *node;

    if ( stores == NULL )
        return;

    pthread_mutex_lock( &widgetMutex );
    if ( status->stores != NULL )
        g_slist_free_full(status->stores, freeStore);
    if ( status->nodes != NULL )
    {
        g_slist_free_full(status->nodes, freeNodes);
        status->nodes = NULL;
    }
    status->stores = stores;
    for (idx=0; idx<g_slist_length(stores); idx++)
    {
        entry    = g_strdup(g_slist_nth_data(stores, idx));
        name     = strtok_r(entry, ":", &savePtr);
        totalStr = strtok_r(NULL, ":", &savePtr);
        availStr = strtok_r(NULL, ":", &savePtr);
                   strtok_r(NULL, ":", &savePtr); /* Skip one pointer; we don't use it. */
        ro_flag  = strtok_r(NULL, ":", &savePtr);
        ps_flag  = strtok_r(NULL, ":", &savePtr);
        label    = strtok_r(NULL, ":", &savePtr);

        if (( entry    == NULL ) ||
            ( name     == NULL ) ||
            ( totalStr == NULL ) ||
            ( availStr == NULL ) ||
            ( ro_flag  == NULL ) ||
            ( ps_flag  == NULL ) 
           )
        {
            piboxLogger(LOG_INFO, "Incomplete data store entry: %s\n", g_slist_nth_data(stores,idx));
            if ( entry )
                g_free(entry);
            continue;
        }
    
        piboxLogger(LOG_TRACE1, "mntdir: %s\n", name);
        listitem = g_slist_find_custom(status->nodes, (gchar *)(name), (GCompareFunc)findByPath);
        if (listitem == NULL)
        {
            node = (struct _statusnodes *)calloc(1, sizeof(struct _statusnodes));

            node->path = (char *)calloc(1, strlen(name)+1);
            memcpy(node->path, name, strlen(name));

            // Partition Label
            if ( label )
            {
                node->label = (char *)calloc(1, strlen(label)+1);
                memcpy(node->label, label, strlen(label));
            }

            // RO Flag
            if ( strcasecmp(ro_flag, "Y") == 0 )
                node->flags |= F_RO_STAMP;

            // PS Flag
            if ( strcasecmp(ps_flag, "Y") == 0 )
                node->flags |= F_PS_STAMP;

            /* Add new node to list */
            status->nodes = g_slist_append(status->nodes, node);
        }
    }
    pthread_mutex_unlock( &widgetMutex );
}

/*
 *========================================================================
 * Name:    gtk_status_start
 * Prototype:   void gtk_status_start( GtkStatus *status )
 *
 * Description:
 * Start the timer that updates the list.
 *========================================================================
 */
void
gtk_status_start(GtkStatus *widget)
{
    /* A timer will update the display. */
    timeout = g_timeout_add(100, (GSourceFunc)timer_exec, NULL);
}

/*
 *========================================================================
 * Name:    gtk_status_end
 * Prototype:   void gtk_status_end( void )
 *
 * Description:
 * Stop the timer that updates the list.
 *========================================================================
 */
void
gtk_status_end()
{
    if ( timeout != 0 )
        g_source_remove(timeout);
}
