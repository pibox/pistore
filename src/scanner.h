/*******************************************************************************
 * pistore
 *
 * scanner.h:  Scan file systems and populate the db.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef SCANNER_H
#define SCANNER_H

/*========================================================================
 * Defined values
 *=======================================================================*/

#define MOUNT_DIR           "/media/usb"

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/


/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef SCANNER_C
extern int isScannerProcessorRunning( void );
extern void startScannerProcessor( void );
extern void shutdownScannerProcessor( void );
extern int  scanFS( char * );
#endif /* !SCANNER_C */
#endif /* !SCANNER_H */
