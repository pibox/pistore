/*******************************************************************************
 * pistore
 *
 * statuswidget.h:  custom widget for showing file status data
 *
 * License: BSD0
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef __STATUSWIDGET_H
#define __STATUSWIDGET_H

#include <gtk/gtk.h>
#include <cairo.h>

G_BEGIN_DECLS

#define STORES_HDR  "RO PS Label Mount Point"
#define S_MOUNTS    "/proc/mounts"

#define GTK_STATUS(obj) GTK_CHECK_CAST(obj, gtk_status_get_type (), GtkStatus)
#define GTK_STATUS_CLASS(klass) GTK_CHECK_CLASS_CAST(klass, gtk_status_get_type(), GtkStatusClass)
#define GTK_IS_STATUS(obj) GTK_CHECK_TYPE(obj, gtk_status_get_type())

typedef struct _GtkStatus GtkStatus;
typedef struct _GtkStatusClass GtkStatusClass;

struct _statusnodes {
    char    *path;
    char    *device;
    char    *label;
    unsigned long rd_last, wr_last;
    unsigned long rd_diff, wr_diff;
    unsigned int  perc;
    unsigned int  flags;
};

struct _GtkStatus {
  GtkDrawingArea parent;
  GdkPixmap *pixmap;
  GdkColor bg;
  GdkColor fg;
  GHashTable *color_hash;
  GSList *stores;
  GSList *nodes;
};

struct _GtkStatusClass {
  GtkDrawingAreaClass parent_class;
};

/* Prototypes */
GtkType gtk_status_get_type(void);
GtkWidget *gtk_status_new(void);
void gtk_status_set( GtkStatus *status, GSList *stores);
void gtk_status_start( GtkStatus *status );
void gtk_status_end( void );

G_END_DECLS

#endif /* __STATUSWIDGET_H */
