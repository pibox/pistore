/*******************************************************************************
 * pistore
 *
 * db.h:  Functions for reading and maintaining directory stats.
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef DB_H
#define DB_H
#include <mntent.h>

/* 
 * ========================================================================
 * Type definitions and Data structures 
 * =======================================================================
 */

#if 0
#define DB_VALID    1
#define DB_INVALID  2
#endif

#define S_EXPORTS           "/etc/exports"
#define S_TEMPLATE_FILE     "/etc/pistore.tmpl"
#define S_TEMPLATE          "%s * (sync)\n"
#define S_RO_STAMP          ".ro"
#define S_PS_STAMP          ".ps"
#define F_RO_STAMP          0x0001
#define F_PS_STAMP          0x0002

typedef struct _fs {
    struct statfs   *statfs;
    char            *mntdir;
} PISTORE_FS_T;

/*
 * A store entry
 */
typedef struct _store_t {
    char    *path;
    char    flags;
    int     total;
    int     used;
} STORE_T;

/*
 * ========================================================================
 * Defined values, some of which are used in test modes only
 * =======================================================================
 */

// Production locations for local media on USB sticks
#define DBTOP       "/media"

// Test location for local media
#define DBTOP_T     "data"

/*
 * ========================================================================
 * Prototypes
 * =======================================================================
 */
#ifndef DB_C
extern void dbUpdateFS (struct mntent *mntpt);
extern void dbRemoveFS (char *path);
extern GSList *dbGetStores ( void );
extern void dbInit ( void );
extern void syncExports( void );
#endif

#endif /* DB_H */
