/*******************************************************************************
 * pistore
 *
 * graphwidget.h:  custom widget for showing file graph data
 *
 * License: BSD0
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef __GRAPHWIDGET_H
#define __GRAPHWIDGET_H

#include <gtk/gtk.h>
#include <cairo.h>

G_BEGIN_DECLS

#define GTK_GRAPH(obj) GTK_CHECK_CAST(obj, gtk_graph_get_type (), GtkGraph)
#define GTK_GRAPH_CLASS(klass) GTK_CHECK_CLASS_CAST(klass, gtk_graph_get_type(), GtkGraphClass)
#define GTK_IS_GRAPH(obj) GTK_CHECK_TYPE(obj, gtk_graph_get_type())

typedef struct _GtkGraph GtkGraph;
typedef struct _GtkGraphClass GtkGraphClass;

struct _GtkGraph {
  GtkDrawingArea parent;
  GdkPixmap *pixmap;
  GdkColor bg;
  GdkColor fg;
  GHashTable *color_hash;
  GSList *stores;
};

struct _GtkGraphClass {
  GtkDrawingAreaClass parent_class;
};

/* Prototypes */
GtkType gtk_graph_get_type(void);
GtkWidget *gtk_graph_new(void);
void gtk_graph_set( GtkGraph *graph, GSList *stores );
void gtk_graph_update( GtkGraph *graph );

G_END_DECLS

#endif /* __GRAPHWIDGET_H */
