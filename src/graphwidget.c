/*******************************************************************************
 * pistore
 *
 * graphwidget.h:  custom widget for showing file graph data
 *
 * License: BSD0
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 * 
 * Based on:
 * Custom GTK+ widget - http://zetcode.com/tutorials/gtktutorial/customwidget/
 * Clock Widget - https://github.com/humbhenri/clocks/tree/master/gtk-clock
 ******************************************************************************/
#define GRAPHWIDGET_C

#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <glib.h>
#include <sys/vfs.h>
#include <pibox/log.h>
#include "graphwidget.h"
#include "pistore.h"

static gint largeFont  = 25;
static gint mediumFont = 15;
static gint smallFont  = 10;

struct _nodes {
    char            *path;
    GdkColor        *bg;
    struct statfs   *statfs;
};

#define KB          (1024)
#define MB          (1024*1024)
#define GB          (1024*1024*1024)
#define S_KB        "KB"
#define S_MB        "MB"
#define S_GB        "GB"

static pthread_mutex_t widgetMutex = PTHREAD_MUTEX_INITIALIZER;

/* Default base color */
GdkColor    base;

/*
 * Local copy of widget structure so some callbacks can
 * access it.
 */
static GtkGraph *local_graph = NULL;

static GSList   *nodes = NULL;

/*
 *========================================================================
 * Prototypes
 *========================================================================
 */
static void gtk_graph_class_init(GtkGraphClass *klass);
static void gtk_graph_init(GtkGraph *graph);
static void gtk_graph_size_request(GtkWidget *widget, GtkRequisition *requisition);
static void gtk_graph_size_allocate(GtkWidget *widget, GtkAllocation *allocation);
static void gtk_graph_realize(GtkWidget *widget);
static gboolean gtk_graph_expose(GtkWidget *widget, GdkEventExpose *event);
static void gtk_graph_paint(GtkWidget *widget);
static void gtk_graph_destroy(GtkObject *object);

static void free_key( gpointer data );
static void free_value( gpointer data );

/*
 *========================================================================
 *========================================================================
 * Private API
 *========================================================================
 *========================================================================
 */
GtkType
gtk_graph_get_type(void)
{
    static GtkType gtk_graph_type = 0;
    if (!gtk_graph_type) {
        static const GtkTypeInfo gtk_graph_info = {
            "GtkGraph",
            sizeof(GtkGraph),
            sizeof(GtkGraphClass),
            (GtkClassInitFunc) gtk_graph_class_init,
            (GtkObjectInitFunc) gtk_graph_init,
            NULL,
            NULL,
            (GtkClassInitFunc) NULL
        };
        gtk_graph_type = gtk_type_unique(GTK_TYPE_WIDGET, &gtk_graph_info);
    }
    return gtk_graph_type;
}

GtkWidget * gtk_graph_new()
{
    return GTK_WIDGET(gtk_type_new(gtk_graph_get_type()));
}

static void
gtk_graph_class_init(GtkGraphClass *klass)
{
    GtkWidgetClass *widget_class;
    GtkObjectClass *object_class;

    widget_class = (GtkWidgetClass *) klass;
    object_class = (GtkObjectClass *) klass;

    widget_class->realize = gtk_graph_realize;
    widget_class->size_request = gtk_graph_size_request;
    widget_class->size_allocate = gtk_graph_size_allocate;
    widget_class->expose_event = gtk_graph_expose;

    object_class->destroy = gtk_graph_destroy;
}

static void
gtk_graph_init(GtkGraph *graph)
{
    graph->stores = NULL;
    graph->pixmap = NULL;
    graph->color_hash = g_hash_table_new_full (
                                g_str_hash,   /* Hash function  */
                                g_str_equal,  /* Comparator     */
                                free_key,     /* Key destructor */
                                free_value);  /* Val destructor */
    base.red   = 20;
    base.green = 20;
    base.blue  = 20;
    local_graph = graph;
}

static void
gtk_graph_size_request(GtkWidget *widget, GtkRequisition *requisition)
{
    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_GRAPH(widget));
    g_return_if_fail(requisition != NULL);
}

static void
gtk_graph_size_allocate(GtkWidget *widget, GtkAllocation *allocation)
{
    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_GRAPH(widget));
    g_return_if_fail(allocation != NULL);

    widget->allocation = *allocation;
    if (GTK_WIDGET_REALIZED(widget)) {
        gdk_window_move_resize(
            widget->window,
            allocation->x, allocation->y,
            allocation->width, allocation->height
        );
    }
}

static void
gtk_graph_realize(GtkWidget *widget)
{
    GdkWindowAttr attributes;
    guint attributes_mask;
    GtkStyle *style;
    GtkAllocation alloc;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_GRAPH(widget));

    GTK_WIDGET_SET_FLAGS(widget, GTK_REALIZED);

    gtk_widget_get_allocation(widget, &alloc);

    piboxLogger(LOG_INFO, "alloc.{x,y,w,h}: %d,%d,%d,%d\n", 
            alloc.x, alloc.y, alloc.width, alloc.height);

    attributes.window_type = GDK_WINDOW_CHILD;
    attributes.x           = alloc.x;
    attributes.y           = alloc.y;
    attributes.width       = alloc.width;
    attributes.height      = alloc.height;
    attributes.wclass      = GDK_INPUT_OUTPUT;
    attributes.event_mask  = gtk_widget_get_events(widget) | GDK_EXPOSURE_MASK;
    attributes_mask        = GDK_WA_X | GDK_WA_Y;

    widget->window = gdk_window_new(
        gtk_widget_get_parent_window (widget),
        & attributes, attributes_mask
    );

    style = gtk_widget_get_style (widget);
    if (style != NULL) 
    {
        GTK_GRAPH(widget)->bg = style->bg[GTK_STATE_NORMAL];
        GTK_GRAPH(widget)->fg = style->fg[GTK_STATE_NORMAL];
    }

    gdk_window_set_user_data(widget->window, widget);
    widget->style = gtk_style_attach(widget->style, widget->window);
    gtk_style_set_background(widget->style, widget->window, GTK_STATE_NORMAL);
}

static gboolean
gtk_graph_expose(GtkWidget *widget, GdkEventExpose *event)
{
    g_return_val_if_fail(widget != NULL, FALSE);
    g_return_val_if_fail(GTK_IS_GRAPH(widget), FALSE);
    g_return_val_if_fail(event != NULL, FALSE);

    // Copy the entire pixmap over the widget's drawing area.
    if ( GTK_GRAPH(widget)->pixmap != NULL )
    {
        gdk_draw_drawable(widget->window,
            widget->style->fg_gc[GTK_WIDGET_STATE(widget)], GTK_GRAPH(widget)->pixmap,
            event->area.x, event->area.y,
            event->area.x, event->area.y,
            event->area.width, event->area.height);
    }
    return TRUE;
}

/*========================================================================
 * Name:   free_key
 * Prototype:  void free_key( gpointer data )
 *
 * Description:
 * Frees up the storage allocated for the deep copy.
 *========================================================================*/
static void
free_key( gpointer data )
{
    g_free(data);
}

/*========================================================================
 * Name:   free_value
 * Prototype:  void free_value( gpointer data )
 *
 * Description:
 * Frees up the storage allocated for the deep copy.
 *========================================================================*/
static void
free_value( gpointer data )
{
    g_free(data);
}

/*
 *========================================================================
 * Name:   findByPath
 * Prototype:  int findByPath( gconstpointer item, gconstpointer user_data )
 *
 * Description:
 * Find FS entry by mount point directory path.
 *========================================================================
 */
static int
findByPath( gconstpointer item, gconstpointer user_data )
{
    struct _nodes *node = (struct _nodes *)item;
    char *path          = (char *)user_data;

    if ( strcmp(node->path, path) == 0 )
        return 0;
    else
        return 1;
}

/*
 *========================================================================
 * Name:    generateRandomColor
 * Prototype:   void generateRandomColor( GdkColor *mix )
 *
 * Description:
 * Generate a new color using the golden ratio.
 * Color mix is returned in the provided arguments.
 *
 * Arguments:
 * GdkColor *mix        Base color to generate the new color from.
 *========================================================================
 */
static void generateRandomColor(GdkColor *mix)
{
    int red   = rand() % 32;
    int green = rand() % 200;
    int blue  = rand() % 256;

    /* mix the color */
    if (mix != NULL) 
    {
        mix->red   = (red + mix->red);
        mix->green = (green + mix->green);
        mix->blue  = (blue + mix->blue);
    }
}

/*
 *========================================================================
 * Name:    freeStore
 * Prototype:   void freeStore( gpointer item )
 *
 * Description:
 * Iterator for freeing up the string for each store.
 *========================================================================
 */
static void
freeStore( gpointer item )
{
    if ( item != NULL )
        free(item);
}

/*
 *========================================================================
 * Name:    gtk_graph_paint
 * Prototype:   void gtk_graph_paint( GtkWidget *widget )
 *
 * Description:
 * Handle painting (data update) of the widget.
 *========================================================================
 */
static void
gtk_graph_paint(GtkWidget *widget)
{
    static int              painting = 0;
    int                     width, height;
    int                     i, idx, bar_width;
    guint                   bars;
    int                     tickstep, offset, endPoint;
    GSList                  *storeList = GTK_GRAPH(widget)->stores;
    GSList                  *entry;
    char                    *str;
    char                    *savePtr;
    char                    *name, *totalStr, *availStr, *bsizeStr;
    unsigned long long      total, avail, bsize;
    unsigned long long      calc;
    char                    *sizetype;
    int                     perc;
    cairo_t                 *cr_pixmap;
    GdkColor                mix; 
    cairo_surface_t         *cst;
    cairo_t                 *cr;
    GdkPixmap               *pixmap;
    cairo_text_extents_t    extents;
    struct _nodes           *node;
    char                    buf[PATH_MAX];
    gint                    fontSize = largeFont;

    /* Avoid multiple calls */
    if ( painting )
        return;

    painting = 1;
    piboxLogger(LOG_TRACE2, "Entered\n");

    if ( isCLIFlagSet( CLI_SMALL_SCREEN ) )
        fontSize = mediumFont;

    if ( GTK_GRAPH(widget)->pixmap != NULL )
        g_object_unref( GTK_GRAPH(widget)->pixmap );
    GTK_GRAPH(widget)->pixmap = 
        gdk_pixmap_new(widget->window,widget->allocation.width,widget->allocation.height,-1);
    pixmap = GTK_GRAPH(widget)->pixmap;

    piboxLogger(LOG_TRACE2, "Getting cr\n");
    gdk_drawable_get_size(pixmap, &width, &height);
    cst = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, width, height);
    cr = cairo_create(cst);

    // Fill with black background with the default color
    cairo_set_source_rgb(cr, 0, 0, 0);
    cairo_rectangle(cr, 0, 0, width, height);
    cairo_fill(cr);

    // Computer width of each bar in the graph.
    if ( storeList != NULL )
    {
        bars = g_slist_length(storeList);
        bar_width = (width - 10) / bars;
    
        cairo_set_line_width(cr, 0.5);
        for (idx=0; idx<g_slist_length(storeList); idx++)
        {
            str = g_strdup(g_slist_nth_data(storeList,idx));
            name     = strtok_r(str, ":", &savePtr);
            totalStr = strtok_r(NULL, ":", &savePtr);
            availStr = strtok_r(NULL, ":", &savePtr);
            bsizeStr = strtok_r(NULL, ":", &savePtr);

            if (( str == NULL   ) ||
                ( name == NULL  ) ||
                ( bsizeStr == NULL ) ||
                ( totalStr == NULL ) ||
                ( availStr == NULL ))
            {
                piboxLogger(LOG_INFO, "Incomplete data store entry: %s\n", g_slist_nth_data(storeList,idx));
                if ( str )
                    g_free(str);
                continue;
            }
    
            total = strtoull(totalStr, NULL, 10);
            avail = strtoull(availStr, NULL, 10);
            bsize = strtoull(bsizeStr, NULL, 10);
            perc  = 100-(int)((float)avail/(float)total*100.0);
            piboxLogger(LOG_TRACE1, "Mount: %s, total=%lu, avail=%lu, size=%lu\n", name, total, avail, bsize);
            piboxLogger(LOG_TRACE1, "Percent used: %d\n", perc);
    
            // Draw the bar.
            entry = g_slist_find_custom(nodes, (gchar *)(name), (GCompareFunc)findByPath);
            node = (struct _nodes *)entry->data;
            memcpy(&mix, node->bg, sizeof(GdkColor));
    
            cairo_set_source_rgb(cr, mix.red/256.0, mix.green/256.0, mix.blue/256.0);
            cairo_rectangle(cr, 10+(idx*bar_width), height-5, bar_width, -(height-10)*((float)perc/100.0));
            cairo_fill(cr);

            /* Write the mount directory, rotated vertically. */
            piboxLogger(LOG_TRACE1, "Writing mount directory name.\n");
            cairo_set_font_size(cr, fontSize);
            cairo_set_source_rgb(cr, 0.6, 0.6, 0.0);

            sprintf(buf, "%s", name);
            cairo_text_extents (cr, buf, &extents);
            cairo_move_to(cr, 5+(idx*bar_width)+(bar_width/2)-extents.height-5, height-10);
            cairo_save(cr);
            cairo_rotate(cr, -1.5708); // 90 degree rotation
            cairo_show_text(cr, buf);
            cairo_restore(cr);

            calc = (unsigned long long)(avail*bsize/GB);
            if ( calc > 0 )
                sizetype=S_GB;
            else
            {
                calc = (unsigned long long)(avail*bsize/MB);
                if ( calc > 0 )
                    sizetype=S_MB;
                else
                {
                    calc = (unsigned long long)(avail*bsize/KB);
                    sizetype=S_KB;
                }
            }
            sprintf(buf, "Available: %llu%s", calc, sizetype);
            piboxLogger(LOG_TRACE1, "%s\n", buf);
            cairo_text_extents (cr, buf, &extents);
            cairo_move_to(cr, 5+(idx*bar_width)+(bar_width/2), height-10);
            cairo_save(cr);
            cairo_rotate(cr, -1.5708); // 90 degree rotation
            cairo_show_text(cr, buf);
            cairo_restore(cr);

            calc = (unsigned long long)(total*bsize/GB);
            if ( calc > 0 )
                sizetype=S_GB;
            else
            {
                calc = (unsigned long long)(total*bsize/MB);
                if ( calc > 0 )
                    sizetype=S_MB;
                else
                {
                    calc = (unsigned long long)(total*bsize/KB);
                    sizetype=S_KB;
                }
            }
            sprintf(buf, "Total: %llu%s", calc, sizetype);
            piboxLogger(LOG_TRACE1, "%s\n", buf);
            cairo_text_extents (cr, buf, &extents);
            cairo_move_to(cr, 5+(idx*bar_width)+(bar_width/2)+extents.height, height-10);
            cairo_save(cr);
            cairo_rotate(cr, -1.5708); // 90 degree rotation
            cairo_show_text(cr, buf);
            cairo_restore(cr);

            if ( str )
                g_free(str);
        }
    }
    else
        piboxLogger(LOG_INFO, "No stores to graph.\n");

    /* overlay axis for the graphs on the mask */
    cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
    cairo_move_to(cr, 10, height-5);
    cairo_line_to(cr, width-10, height-5);
    cairo_stroke(cr);

    cairo_move_to(cr, 10, height-5);
    cairo_line_to(cr, 10, 5);
    cairo_stroke(cr);

    /* Ticks and labels for the graphs */
    if ( isCLIFlagSet( CLI_SMALL_SCREEN ) )
        endPoint = 10;
    else
        endPoint = width-10;
    cairo_set_font_size(cr, smallFont);
    tickstep = (height-10) / 10;
    offset = 0;
    perc=90;
    cairo_set_line_width(cr, 0.5);
    for(i=0; i<10; i++)
    {
        cairo_move_to(cr, 0, offset);
        cairo_line_to(cr, endPoint, offset);
        cairo_stroke(cr);
        offset += tickstep;

        sprintf(buf, "%d%%", perc);
        cairo_move_to(cr, 10, offset-3);
        cairo_save(cr);
        cairo_rotate(cr, -1.5708); // 90 degree rotation
        cairo_show_text(cr, buf);
        cairo_restore(cr);
        perc -= 10;
    }

    /* Don't need the cairo object now */
    cairo_destroy(cr);

    cr_pixmap = gdk_cairo_create(pixmap);
    cairo_set_source_surface (cr_pixmap, cst, 0, 0);
    cairo_paint(cr_pixmap);
    cairo_destroy(cr_pixmap);
    cairo_surface_destroy(cst);

    painting = 0;
}

/*
 *========================================================================
 * Name:    gtk_graph_destroy
 * Prototype:   void gtk_graph_destroy( GtkWidget *widget )
 *
 * Description:
 * Clean up widget on destroy.
 *========================================================================
 */
static void
gtk_graph_destroy(GtkObject *object)
{
    GtkGraphClass *klass;

    g_return_if_fail(object != NULL);
    g_return_if_fail(GTK_IS_GRAPH(object));

    klass = gtk_type_class(gtk_widget_get_type());
    if (GTK_OBJECT_CLASS(klass)->destroy) {
        (* GTK_OBJECT_CLASS(klass)->destroy) (object);
    }
}

/*
 *========================================================================
 *========================================================================
 * Public API
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:    gtk_graph_set
 * Prototype:   void gtk_graph_set( GtkGraph *graph, GSList *stores )
 *
 * Description:
 * Set the list of stores to graph.
 *========================================================================
 */
void
gtk_graph_set( GtkGraph *graph, GSList *stores )
{
    char            *entry, *name, *totalStr, *availStr, *savePtr;
    GSList          *listitem;
    GdkColor        color;
    struct _nodes   *node;
    int             idx;
    int             rc;
    struct statfs   fs;

    pthread_mutex_lock( &widgetMutex );
    if ( graph->stores != NULL )
        g_slist_free_full(graph->stores, freeStore);
    graph->stores = stores;
    for (idx=0; idx<g_slist_length(stores); idx++)
    {
        entry    = g_strdup(g_slist_nth_data(stores, idx));
        name     = strtok_r(entry, ":", &savePtr);
        totalStr = strtok_r(NULL, ":", &savePtr);
        availStr = strtok_r(NULL, ":", &savePtr);

        if (( entry == NULL   ) ||
            ( name == NULL  ) ||
            ( totalStr == NULL ) ||
            ( availStr == NULL ))
        {
            piboxLogger(LOG_INFO, "Incomplete data store entry: %s\n", g_slist_nth_data(stores,idx));
            if ( entry )
                g_free(entry);
            continue;
        }
    
        piboxLogger(LOG_TRACE1, "mntdir: %s\n", name);
        listitem = g_slist_find_custom(nodes, (gchar *)(name), (GCompareFunc)findByPath);
        if (listitem == NULL)
        {
            rc = statfs(name, &fs);
            if ( rc != 0 )
                return;

            node = (struct _nodes *)calloc(1, sizeof(struct _nodes));
            color.red   = base.red;
            color.green = base.green;
            color.blue  = base.blue;
            generateRandomColor(&color);
            node->path = (char *)calloc(1, strlen(name)+1);
            node->bg = (GdkColor *)calloc(1, sizeof(GdkColor));
            node->statfs = (struct statfs *)calloc(1, sizeof(struct statfs));
            memcpy((char *)(node->statfs), &fs, sizeof(struct statfs));
            memcpy(node->path, name, strlen(name));
            memcpy((char *)node->bg, (char *)&color, sizeof(GdkColor));
            nodes = g_slist_append(nodes, node);
        }
    }
    pthread_mutex_unlock( &widgetMutex );
}

/*
 *========================================================================
 * Name:    gtk_graph_update
 * Prototype:   void gtk_graph_update( GtkGraph *graph )
 *
 * Description:
 * Update the graphs based on current list of stores.
 *========================================================================
 */
void
gtk_graph_update( GtkGraph *graph )
{
    GdkRegion *region;

    pthread_mutex_lock( &widgetMutex );
    gtk_graph_paint(GTK_WIDGET(graph));
    region = gdk_drawable_get_clip_region(GTK_WIDGET(graph)->window);
    gdk_window_invalidate_region(GTK_WIDGET(graph)->window, region, TRUE);
    gdk_window_process_updates(GTK_WIDGET(graph)->window, TRUE);
    pthread_mutex_unlock( &widgetMutex );
}
