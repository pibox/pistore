/*******************************************************************************
 * pistore
 *
 * utils.c:  Various utility methods
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define UTILS_C

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <uuid.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <X11/Xlib.h>
#include <pibox/pibox.h>
#include <pibox/utils.h>

#include "pistore.h"



/*========================================================================
 * Name:   piboxGetXDisplaySize
 * Prototype:  void piboxGetXDisplaySize( int *x, int *y )
 *
 * Description:
 * Get the display size in x and y dimensions.
 *
 * Returns:
 * The x and y dimenions to the input arguments.
 *========================================================================*/
void
piboxGetXDisplaySize ( int *x, int *y )
{
    Display *dpy; 
    int snum;

    if(!(dpy = XOpenDisplay(0)))
        piboxLogger(LOG_INFO, "Cannot open display to get display dimensions.\n");
    snum = DefaultScreen(dpy);
    *x = DisplayWidth(dpy, snum);
    *y = DisplayHeight(dpy, snum);
    XCloseDisplay(dpy);
}
