/*******************************************************************************
 * pistore
 *
 * pistore.h:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef PISTORE_H
#define PISTORE_H

#include <gtk/gtk.h>

#define KEYSYMS_F       "/etc/pibox-keysyms"
#define KEYSYMS_FD      "data/pibox-keysyms"
#define F_IMAGE_DIR     "/etc/launcher/icons"
#define F_IMAGE_DIR_T   "data/icons"

// PID file: Also referenced by the associated init script.
#define PIDFILE         "/var/run/pistore.pid"

/*========================================================================
 * Globals
 *=======================================================================*/
#ifdef PISTORE_C
char    errBuf[256];
extern char     errBuf[];
#endif /* PISTORE_C */

/*========================================================================
 * Defined values
 *=======================================================================*/
#define PROG        "pistore"
#define MAXBUF      4096

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef PISTORE_C
#endif

/*========================================================================
 * Include other headers
 *=======================================================================*/
#include <pibox/log.h>
#include "cli.h"
#include "utils.h"
#include "scanner.h"
#include "db.h"

#endif /* !PISTORE_H */

