/*******************************************************************************
 * pistore
 *
 * pistore.c:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define PISTORE_C

#include <stdlib.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <uuid/uuid.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <pibox/pibox.h>
#include <pibox/utils.h>

#include "pistore.h"
#include "graphwidget.h"
#include "statuswidget.h"

#define GRAPH_PAD   5

/* Local prototypes */
gboolean imageTouchGTK( int region );
void createPopup( void );
void updateStatus( void );

GtkWidget           *darea;
GdkPixmap           *pixmap_dest = NULL;
cairo_t             *cr = NULL;
guint               homeKey = -1;
GtkWidget           *window;
GtkWidget           *popup = NULL;
GtkWidget           *localStores;
GtkWidget           *networkStores;
int                 popup_state = 0;
int                 inprogress = 0;

/* 
 * Font sizes depend on screen size.
 * Defaults to a normal sized screen.
 * Small screens reduce this size.
static gint largeFont  = 25;
static gint mediumFont = 15;
static gint smallFont  = 10;
static gint summaryFont= 20;
static gint listFont   = 20;
 */

/*
 *========================================================================
 * Name:   togglePopup
 * Prototype:  void togglePopup( void )
 *
 * Description:
 * Displays and hides the popup window showing the current set of exports.
 *========================================================================
 */
static void
togglePopup()
{
    if ( popup == NULL )
    {
        createPopup();
        popup_state = 1;
    }
    else
    {
        if ( popup_state == 1 )
        {
            gtk_widget_hide_all(popup);
            popup_state = 0;
        }
        else
        {
            updateStatus();
            gtk_widget_show_all(popup);
            popup_state = 1;
        }
    }
}

/*
 *========================================================================
 * Name:   imageTouch
 * Prototype:  void imageTouch( int region )
 *
 * Description:
 * Handler for absolute touch reports.  For PiStore touches either
 * move to a different page or exit the app.
 *
 *   -----------------------------------
 *   | 0: Toggle | 1: Toggle | 2: Quit | 
 *   -----------------------------------
 *   | 3: Toggle | 4: Toggle | 5: Quit |
 *   -----------------------------------
 *   | 6: Toggle | 7: Toggle | 8: Quit |
 *   -----------------------------------
 *========================================================================
 */
void
imageTouch( int region )
{
    g_idle_add( (GSourceFunc)imageTouchGTK, GINT_TO_POINTER(region) );
}

gboolean
imageTouchGTK( int region )
{
    /* Prevent multiple touches from being handled at the same time. */
    if ( inprogress )
    {
        piboxLogger(LOG_INFO, "In progress, skipping update\n");
        return FALSE;
    }
    inprogress = 1;

    piboxLogger(LOG_INFO, "Touch region: %d\n", region);
    switch( region )
    {
        /* Quit */
        case 2:
        case 5:
        case 8:
            piboxLogger(LOG_INFO, "Quit region\n");
            gtk_main_quit();
            break;

        /* Toggle popup */
        default:
            piboxLogger(LOG_INFO, "Toggle\n");
            togglePopup();
            break;
    }
    inprogress = 0;
	return(FALSE);
}

/*
 *========================================================================
 * Name:   loadKeysyms
 * Prototype:  void loadKeysyms( void )
 *
 * Description:
 * Read in the keysym file so we know how the platform wants us to behave.
 *
 * Notes:
 * Format is KEYSYM NAME:ACTION
 *========================================================================
 */
void
loadKeysyms( void )
{
    char        *tsave = NULL;
    struct stat stat_buf;
    char        *keysym;
    char        *action;
    char        *ptr;
    FILE        *fd;
    char        buf[128];
    char        *path;

    if ( isCLIFlagSet( CLI_TEST) )
        path = KEYSYMS_FD;
    else
        path = KEYSYMS_F;

    /* Read in /etc/pibox-keysysm */
    if ( stat(path, &stat_buf) != 0 )
    {
        piboxLogger(LOG_INFO, "No keysym file: %s\n", path);
        return;
    }

    fd = fopen(path, "r");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed to open keysyms file: %s - %s\n", path, strerror(errno));
        return;
    }

    memset(buf, 0, 128);
    while( fgets(buf, 127, fd) != NULL )
    {
        /* Ignore comments */
        if ( buf[0] == '#' )
            continue;

        /* Strip leading white space */
        ptr = buf;
        ptr = piboxTrim(ptr);

        /* Ignore blank lines */
        if ( strlen(ptr) == 0 )
            continue;

        /* Strip newline */
        piboxStripNewline(ptr);

        /* Grab first token */
        keysym = strtok_r(ptr, ":", &tsave);
        if ( keysym == NULL )
            continue;

        /* Grab second token */
        action = strtok_r(NULL, ":", &tsave);
        if ( action == NULL )
            continue;

        piboxLogger(LOG_INFO, "keysym / action: %s / %s \n", keysym, action);

        /* Set the home key */
        if ( strncasecmp("home", action, 4) == 0 )
        {
            homeKey = gdk_keyval_from_name(keysym);
            piboxLogger(LOG_INFO, "homeKey = %08x\n", homeKey);
        }
    }
}

/*
 *========================================================================
 * Name:   loadDisplayConfig
 * Prototype:  void loadDisplayConfig( void )
 *
 * Description:
 * Read in the display config file so we know how the display should be
 * handled.
 *
 * Notes:
 * Format is
 *     DISPLAY TYPE (DVT LCD)
 *     RESOLUTION
 *========================================================================
 */
void
loadDisplayConfig( void )
{
    int width;
    int height;

    piboxLoadDisplayConfig();
    width = piboxGetDisplayWidth();
    height = piboxGetDisplayHeight();

    if  (width == 0 )
    {
        piboxGetDisplaySize ( &width, &height );
        piboxLogger(LOG_INFO, "Width x Height (fb): %d x %d\n", width, height);
    }
    piboxLogger(LOG_INFO, "Display dimensions: %d x %d.\n", width, height);

    if ( (width<=800) || (height<=480) || (piboxGetDisplayType() == PIBOX_TFT) )
    {
        setCLIFlag( CLI_SMALL_SCREEN );
    }

    if ( piboxGetDisplayTouch() == PIBOX_TOUCH )
    {
        setCLIFlag(CLI_TOUCH);
        piboxLogger(LOG_INFO, "Touch screen support enabled.\n");
    }
    else
    {
        unsetCLIFlag(CLI_TOUCH);
        piboxLogger(LOG_INFO, "Not a touch screen.\n");
    }
}

/*
 *========================================================================
 * Name:   key_press
 * Prototype:  void key_press( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Handles exiting the application via keystrokes.
 *========================================================================
 */
static gboolean
key_press(GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
    switch(event->keyval)
    {
        case GDK_KEY_Q:
        case GDK_KEY_q:
            if (event->state & GDK_CONTROL_MASK)
            {
                piboxLogger(LOG_INFO, "Ctrl-Q key\n");
                gtk_main_quit();
                return(TRUE);
            }
            break;

        case GDK_KEY_Home:
            piboxLogger(LOG_INFO, "Home key\n");
            gtk_main_quit();
            return(TRUE);
            break;

        case GDK_KEY_Tab:
            piboxLogger(LOG_INFO, "Tab key\n");
            togglePopup();
            return(TRUE);
            break;

        default:
            if ( event->keyval == homeKey )
            {
                piboxLogger(LOG_INFO, "Keysym configured home key\n");
                gtk_main_quit();
                return(TRUE);
            }
            else
            {
                piboxLogger(LOG_INFO, "Unknown keysym: %s\n", gdk_keyval_name(event->keyval));
                return(FALSE);
            }
            break;
    }
    return(FALSE);
}

/*
 *========================================================================
 * Name:   updateGraphs
 * Prototype:  void updateGraphs( void )
 *
 * Description:
 * Updates the storage graph widgets.
 *========================================================================
 */
void
updateGraphs( void )
{
    GSList  *local = NULL;

    // Rebuild the store lists
    local = dbGetStores();
    gtk_graph_set( GTK_GRAPH(localStores), local );
    gtk_graph_update( GTK_GRAPH(localStores) );
}

/*
 *========================================================================
 * Name:   updateStatus
 * Prototype:  void updateStatus( void )
 *
 * Description:
 * Updates the status widget.
 *========================================================================
 */
void
updateStatus( void )
{
    GSList  *local = NULL;

    // Rebuild the store lists
    local = dbGetStores();

    // Set the store lists in the widgets
    gtk_status_set( GTK_STATUS(darea), local);
}

/*
 *========================================================================
 * Name:   on_window_expose_event
 * Prototype:  void on_window_expose_event( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Update an exposed area with the associated region from the pixmap.
 *========================================================================
 */
gboolean
on_window_expose_event( GtkWidget * da, GdkEventExpose * event, gpointer user_data )
{
    updateGraphs();
    return TRUE;
}

/*
 *========================================================================
 * Name:   createPopup
 * Prototype:  void createPopup( void )
 *
 * Description:
 * Creates the popup window that shows configuration information for shares.
 *========================================================================
 */
void
createPopup( void )
{
    GtkWidget   *vbox;
    gint        width, height;

    popup = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    GTK_WIDGET_SET_FLAGS(popup, GTK_CAN_FOCUS );
    gtk_widget_add_events(popup, GDK_KEY_PRESS_MASK);
    g_signal_connect(G_OBJECT(popup),
        "key_press_event",
        G_CALLBACK(key_press),
        NULL);

    vbox = gtk_vbox_new (TRUE, 0);
    gtk_widget_set_name (vbox, "vbox");
    gtk_widget_show (vbox);
    gtk_container_add (GTK_CONTAINER (popup), vbox);

    /*
     * The status bar
     */
    if  (piboxGetDisplayHeight() == 0 )
    {
        piboxGetDisplaySize ( &width, &height );
        piboxLogger(LOG_INFO, "Width x Height (fb): %d x %d\n", width, height);
    }
    else
    {
        height = (gint)piboxGetDisplayHeight();
        width = (gint)piboxGetDisplayWidth();
    }
    piboxLogger(LOG_INFO, "Status bar height: %d\n", height);

    darea = gtk_status_new();
    gtk_box_pack_start (GTK_BOX (vbox), darea, TRUE, TRUE, 0);
    gtk_widget_show (darea);

    gtk_window_set_position(GTK_WINDOW(popup), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(popup), width, height);
    gtk_widget_show_all(popup);
    updateStatus();
    gtk_status_start(GTK_STATUS(darea));
}

/*
 *========================================================================
 * Name:   createWindow
 * Prototype:  GtkWidget *createWindow( void )
 *
 * Description:
 * Creates the main window with a list of videos on the left and currently
 * selected video poster on the right.  The poster area is sensitive to
 * clicks that will start playing the video.
 *========================================================================
 */
GtkWidget *
createWindow()
{
    GtkWidget   *vbox;
    GtkWidget   *hbox;
    gint        width, height;

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    GTK_WIDGET_SET_FLAGS(window, GTK_CAN_FOCUS );
    gtk_widget_add_events(window, GDK_KEY_PRESS_MASK);
    g_signal_connect(G_OBJECT(window),
                "key_press_event",
                G_CALLBACK(key_press),
                NULL);

    vbox = gtk_vbox_new (FALSE, 0);
    gtk_widget_set_name (vbox, "vbox");
    gtk_widget_show (vbox);
    gtk_container_add (GTK_CONTAINER (window), vbox);

    if  (piboxGetDisplayHeight() == 0 )
    {
        piboxGetDisplaySize ( &width, &height );
        piboxLogger(LOG_INFO, "Width x Height (fb): %d x %d\n", width, height);
        // height = (gint)(height * 0.1);
    }
    else
    {
        height = (gint)piboxGetDisplayHeight();
        width = (gint)piboxGetDisplayWidth();
    }
    piboxLogger(LOG_INFO, "Window height: %d\n", height);

    hbox = gtk_hbox_new (FALSE, 0);
    gtk_widget_set_size_request( hbox, width, height);
    gtk_widget_set_name (hbox, "hbox");
    gtk_widget_show (hbox);
    gtk_box_pack_start (GTK_BOX (vbox), hbox, TRUE, TRUE, 0);

    /*
     * The graph widgets
     */
    localStores = gtk_graph_new();
    gtk_box_pack_start (GTK_BOX (hbox), localStores, TRUE, TRUE, 0);
    gtk_widget_show (localStores);

    /* Make the main window die when destroy is issued. */
    g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK (gtk_main_quit), NULL);

    /* Now position the window and set its title */
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(window), width, height);
    gtk_window_set_title(GTK_WINDOW(window), "pistore");

    return window;
}

/*
 * ========================================================================
 * Name:   timer_exec
 * Prototype:  gboolean timer_exec( GtkWidget *window )
 *
 * Description:
 * Update storage graphs periodically.
 * ========================================================================
 */
static gboolean 
timer_exec(GtkWidget * window)
{
    /* Update the display. */
    updateGraphs();
    return TRUE;
}

/*
 * ========================================================================
 * Name:   one_shot
 * Prototype:  gboolean one_shot( GtkWidget *window )
 *
 * Description:
 * Initial update to the graph window.
 * ========================================================================
 */
static gboolean 
one_shot(GtkWidget * window)
{
    /* Update the display. */
    updateGraphs();
    return FALSE;
}

/*
 * ========================================================================
 * Name:   main
 *
 * Description:
 * Program startup
 * ========================================================================
 */
int
main(int argc, char *argv[])
{
    char    cwd[512];
    char    gtkrc[1024];
    guint   timeout = 0;

    GtkWidget *window;

    /* Load saved configuration and parse command line */
    initConfig();
    parseArgs(argc, argv);
    validateConfig();

    /* Setup logging */
    piboxLoggerInit(cliOptions.logFile);
    piboxLoggerVerbosity(cliOptions.verbose);
    printf("Verbosity level: %d\n", piboxLoggerGetVerbosity());

    if ( cliOptions.logFile != NULL )
    {
        piboxLogger(LOG_INFO, "Log file: %s\n", cliOptions.logFile);
    }
    else
    {
        piboxLogger(LOG_INFO, "No log file configured.\n");
    }
    
    /* Get the current working directory. */
    memset(cwd, 0, 512);
    getcwd(cwd, 512);

    /* Read environment config for keyboard behaviour */
    loadKeysyms();

    /* Get display config information */
    loadDisplayConfig();

    /*
     * If we're on a touchscreen, register the input handler.
     */
    if ( isCLIFlagSet(CLI_TOUCH) )
    {
        piboxLogger(LOG_INFO, "Registering imageTouch.\n");
        piboxTouchRegisterCB(imageTouch, TOUCH_REGION);
        piboxTouchStartProcessor();
    }

    /* Initialize db */
    dbInit();

    /* Initialize file system database. Ignore rc from scanFS(). */
    (void)scanFS(NULL);

    /* Synchronize current exports file with current available filesystems. */
    syncExports();

    /*
     * We need to initialize all these functions so that gtk knows
     * to be thread-aware.
     */
    gdk_threads_init();
    gdk_threads_enter();

    gtk_init(&argc, &argv);
    if ( isCLIFlagSet( CLI_TEST) )
        gtk_rc_parse(gtkrc);
    else
        piboxLogger(LOG_INFO, "CLI_TEST is not set.\n");
    window = createWindow();
    gtk_widget_show_all(window);

    /* A timer will update the display. */
    (void)g_timeout_add(100, (GSourceFunc)one_shot, NULL);
    timeout = g_timeout_add(2000, (GSourceFunc)timer_exec, NULL);

    /* Start threads */
    startScannerProcessor();

    gtk_main();
    gdk_threads_leave();

    /* Remove the repeating timer. */
    g_source_remove(timeout);

    /* Stop the status updates. */
    gtk_status_end();

    /* Stop threads */
    shutdownScannerProcessor();

    if ( isCLIFlagSet(CLI_TOUCH) )
    {
        /* SIGINT is required to exit the touch processor thread. */
        raise(SIGINT);
        piboxTouchShutdownProcessor();
    }

    piboxLoggerShutdown();
    return 0;
}

