#!/bin/bash -p
# Functions and aliases used to nagivate project directories.
# Include by the ~/bin/cdtools script.
#############################################################################

# -------------------------------------------------------------------
# DESC: PiStore - AP app used to manage file systems.
# -------------------------------------------------------------------
function pistore {
    # -------------------------------------------------------------------
    # Edit these items
    # -------------------------------------------------------------------

    # Top of the source tree
    # The project directory will be placed under SRCTOP
    SRCTOP=<top of projects source tree>

    # -------------------------------------------------------------------
    # Don't edit below here
    # -------------------------------------------------------------------

    # Project name
	PARENT=pibox

    if [ "$1" != "" ]
    then
        case "$1" in
        "web")  PRJ=pistore$1;;
        *) 
            echo "Invalid repo"
            pistorerepos
            return 0
            ;;
        esac
    else
    PRJ=pistore
    fi

    # Repository
    export GITREPO=git@gitlab.com:$PARENT/$PRJ.git

    # Suffix allows for creating multiple trees for the same repo
    if [ "$1" != "" ]
    then
        SFX=$2
    else
        SFX=$1
    fi

    # Create top level directory, if needed
    mkdir -p $SRCTOP

    # Where I do my dev work
    GM_WORK=$SRCTOP/work
    mkdir -p $GM_WORK

    # Where the SCM is located
    GM_HOME=$SRCTOP/$PRJ$SFX

    # Where the source and build directories live
    GM_SRC=$GM_HOME/src
    GM_BUILD=$GM_HOME/bld

    # Archives, packaging extras
    GM_ARCHIVE=$GM_HOME/archive
    GM_PKG=$GM_HOME/pkg
    GM_EXTRAS=$GM_HOME/extras

    # Make the configured environment available 
    export GM_WORK
    export GM_ARCHIVE
    export GM_HOME
    export GM_SRC
    export GM_BUILD
    export GM_PKG
    export GM_EXTRAS

    # Some aliases to bounce around directories easily
    alias cdt='cd $SRCTOP'
    alias cdh='cd $GM_HOME'
    alias cdw='cd $GM_WORK'
    alias cdx='cd $GM_SRC'
    alias cdb='cd $GM_BUILD'
    alias cdp='cd $GM_PKG'
    alias cda='cd $GM_ARCHIVE'
    alias cde='cd $GM_EXTRAS'
    alias cdl='ironmanrepos'

    # Show the aliases for this configuration
    alias cd?='listpistore'
}
function listpistore {
echo "
$PRJ Alias settings:
-----------------------------------------------------------------------------
cdt    cd SRCTOP ($SRCTOP)
cdh    cd GM_HOME ($GM_HOME)
cdw    cd GM_WORK ($GM_WORK)
cdx    cd GM_SRC ($GM_SRC)
cdb    cd GM_BUILD ($GM_BUILD)
cdp    cd GM_PKG ($GM_PKG)
cda    cd GM_ARCHIVE ($GM_ARCHIVE)
cde    cd GM_EXTRAS ($GM_EXTRAS)

Repository: $REPO

To checkout tree:
cdt
mkdir $PRJ$SFX
cdh
git clone $GITREPO src
"
}
function pistorerepos {
echo "
$PRJ repos:
--------------------------------------------------------------------------------
no subproject:     pistore App
web          :     RESTful Web - used for interaction with Ironman
"
}
